- [Building Large-Scale Internet Services @ Google](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/44877.pdf)
- [Cluster-Level Storage @ Google](http://www.pdsw.org/pdsw-discs17/slides/PDSW-DISCS-Google-Keynote.pdf)

---

- [Linkedin Ambry](https://github.com/linkedin/ambry/wiki)
- [Apache Kafka Design](https://kafka.apache.org/documentation/#design)
- [LogDevice: a distributed data store for logs](https://code.fb.com/core-data/logdevice-a-distributed-data-store-for-logs/), [Architecture](https://logdevice.io/docs/Concepts.html)
- [CocroachDB Architecture](https://www.cockroachlabs.com/docs/stable/architecture/overview.html), [CockroachDB: The Resilient Geo-Distributed SQL Database](https://www.cockroachlabs.com/guides/cockroachdb-the-resilient-geo-distributed-sql-database-sigmod-2020/)
- [TiKV Deep Dive](https://tikv.org/deep-dive/rpc/introduction/), [TiDB: A Raft-based HTAP Database](http://www.vldb.org/pvldb/vol13/p3072-huang.pdf)
- [Consul Architecture](https://www.consul.io/docs/internals/architecture.html)
- [FaunaDB Architectural Overview](https://fauna-assets.s3.amazonaws.com/public/FaunaDB-Technical-Whitepaper.pdf)