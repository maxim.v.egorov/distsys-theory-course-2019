# System Design Seminal Papers

- [MapReduce: Simplified Data Processing on Large Clusters](https://ai.google/research/pubs/pub62)
- [The Google File System](https://ai.google/research/pubs/pub51), [GFS: Evolution on Fast-forward](https://queue.acm.org/detail.cfm?id=1594206)
- [Dynamo: Amazon’s Highly Available Key-value Store](https://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf)
- [Bigtable: A Distributed Storage System for Structured Data](https://ai.google/research/pubs/pub27898)
- [The Chubby lock service for loosely-coupled distributed systems](https://ai.google/research/pubs/pub27897)
- [ZooKeeper: Wait-free coordination for Internet-scale systems](https://www.usenix.org/legacy/event/atc10/tech/full_papers/Hunt.pdf)
- [Large-scale Incremental Processing Using Distributed Transactions and Notifications](https://ai.google/research/pubs/pub36726)
- [Spanner: Google's Globally-Distributed Database](https://ai.google/research/pubs/pub39966)
- [Amazon Aurora](https://www.allthingsdistributed.com/files/p1041-verbitski.pdf)
